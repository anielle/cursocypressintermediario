/// <reference types="Cypress" />

Cypress.Commands.add('cloneViaSSH', projeto => {
    const domain = Cypress.config('baseUrl').replace('http://', '').replace('/', '')
  
    cy.exec(`cd temp/ && git clone git@${domain}:${Cypress.env('user_name')}/${projeto.nome}.git`)
  })
  