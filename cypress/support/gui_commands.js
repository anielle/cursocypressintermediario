///<reference types="Cypress" />

Cypress.Commands.add('gui_login', ()=>{
    cy.visit('/users/sign_in')
    cy.get('[data-qa-selector="login_field"]').type(Cypress.env('user_name'));
    cy.get('[data-qa-selector="password_field"]').type(Cypress.env('user_password'));
    cy.get('[data-qa-selector="sign_in_button"]').click();
})

Cypress.Commands.add('gui_logout', ()=>{
    cy.get('.header-user-dropdown-toggle').click();
    cy.get('[data-qa-selector="sign_out_link"]').click();
})

Cypress.Commands.add('gui_criarProjeto', projeto=>{
    cy.visit('projects/new')
    cy.get('#project_name').type(projeto.nome)
    cy.get('#project_description').type(projeto.descricao)
    cy.get('.qa-initialize-with-readme-checkbox').check()
    cy.contains('Create project').click()
})

Cypress.Commands.add('gui_criarIssue', issue=>{
    cy.visit(`${Cypress.env('user_name')}/${issue.projeto.nome}/issues/new`)

    cy.get('.qa-issuable-form-title').type(issue.titulo)
    cy.get('.qa-issuable-form-description').type(issue.descricao)
    cy.contains('Submit issue').click()
})

Cypress.Commands.add('gui_setLabelOnIssue', label =>{
    cy.get('.qa-edit-link-labels').click()
    cy.contains(label.nome).click()
    cy.get('body').click()
})

Cypress.Commands.add('gui_setMilestoneOnIssue', milestone=>{
    cy.get('.block.milestone .edit-link').click()
    cy.contains(milestone.titulo).click() 
})
