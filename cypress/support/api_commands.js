///<reference types="Cypress" />

const acessToken = Cypress.env('gitlab_access_token')

Cypress.Commands.add('api_criarProjeto', (projeto)=>{
    cy.request({
        method:`POST`,
        url:`/api/v4/projects/?private_token=${acessToken}`,
        body: {
            name: projeto.nome,
            description: projeto.descricao,
            initialize_with_readme: true
        }
    })
})

Cypress.Commands.add('api_criarIssue', issue =>{
    cy.api_criarProjeto(issue.projeto).then(response=>{
        cy.request({
            method:`POST`,
            url:`/api/v4/projects/${response.body.id}/issues?private_token=${acessToken}`,
            body:{
                title: issue.titulo,
                description: issue.descricao
            }
        })
    })
})

Cypress.Commands.add('api_criarLabel', (projectId, label) => {
    cy.request({
        method:'POST',
        url:`/api/v4/projects/${projectId}/labels?private_token=${acessToken}`,
        body:{
            name:label.nome,
            color:label.color
        }
    })
})


Cypress.Commands.add('api_criarMilestone',(projectId, milestone)=>{
    cy.request({
        method:`POST`,
        url:`/api/v4/projects/${projectId}/milestones?private_token=${acessToken}`,
        body:{
            title: milestone.titulo 
        }
    })
})

