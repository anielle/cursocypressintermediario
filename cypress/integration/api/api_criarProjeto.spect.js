///<reference types="Cypress" />

const faker= require('faker')

describe('Criacao Projeto via API', ()=>{
    it('Criacao Projeto Sucesso', ()=>{
        const projeto = {
            nome: `project-${faker.random.uuid()}`,
            descricao: faker.random.words(5)
        }

        cy.api_criarProjeto(projeto).then(response =>{
            expect(response.status).to.equal(201)
            expect(response.body.name).to.equal(projeto.nome)
            expect(response.body.description).to.equal(projeto.descricao)
        })

    })

})