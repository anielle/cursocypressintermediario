///<reference types="Cypress" />

const faker = require('faker')

describe('Criacao Issue via API', ()=>{
    it('criar issue com sucesso', ()=>{
        const issue= {
            titulo:`issue-${faker.random.uuid()}`,
            descricao: faker.random.words(3),
            projeto: {
                nome:`projeto-${faker.random.uuid()}`,
                descricao: faker.random.words(5)
            }
        }
        
        cy.api_criarIssue(issue).then(response => {
            expect(response.status).to.equal(201)
            expect(response.body.title).to.equal(issue.titulo)
            expect(response.body.description).to.equal(issue.descricao)
        })

    })
})