/// <reference types="Cypress" />

const faker = require('faker')

describe('git clone', () => {
  const projeto = {
    nome: `project-${faker.random.uuid()}`,
    descricao: faker.random.words(5)
  }

  beforeEach(() => cy.api_criarProjeto(projeto))

  it('successfully', () => {
    cy.cloneViaSSH(projeto)

    cy.readFile(`temp/${projeto.nome}/README.md`)
      .should('contain', `# ${projeto.nome}`)
      .and('contain', projeto.descricao)
  })
})
