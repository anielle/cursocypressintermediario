///<reference types="Cypress" />

const faker = require('faker');

describe('Criacao de Issue',()=>{
    const issue={
        titulo:`issue-${faker.random.uuid()}`,
        descricao: faker.random.words(3),
        projeto: {
            nome:`projeto-${faker.random.uuid()}`,
            descricao: faker.random.words(5)
        }
    }
    
    beforeEach(()=>{
        cy.gui_login()
        //cy.gui_criarProjeto(issue.projeto)
        cy.api_criarProjeto(issue.projeto)
    })

    it('criacao Issue com sucesso', ()=>{
        cy.gui_criarIssue(issue)

        cy.get('.issue-details')
        .should('contain', issue.titulo)
        .and('contain', issue.descricao)
    })
})