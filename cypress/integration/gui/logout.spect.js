///<reference types="Cypress" />

describe('Logout', ()=>{
    beforeEach(()=>cy.gui_login())

    it('logout com sucesso', ()=>{
        cy.gui_logout();

        cy.url().should('be.equal',`${Cypress.config('baseUrl')}users/sign_in`)

    })
})