///<reference types="Cypress" />

const faker = require ('faker')

describe('Inserindo uma milestone ao projeto',()=>{
    const issue={
        titulo: `issue-${faker.random.uuid()}`,
        descricao: faker.random.words(3),
        projeto:{
            nome:`projeto-${faker.random.uuid()}`,
            descricao: faker.random.words(5)
        }
    }

    const milestone={
        titulo:`miletone-${faker.random.word()}`
    }

    beforeEach(()=>{
        cy.gui_login()
        cy.api_criarIssue(issue)
        .then(response=>{
            cy.api_criarMilestone(response.body.project_id, milestone)
            cy.visit(`${Cypress.env('user_name')}/${issue.projeto.nome}/issues/${response.body.iid}`)
        })
    })

    it('add milestone ao projeto com sucesso',()=>{
        cy.gui_setMilestoneOnIssue(milestone)

        cy.get('.hide-collapsed').should('contain', milestone.titulo)
    })
    
})