///<reference types="Cypress" />

const faker = require ('faker')

describe('Inserindo label em uma issue', ()=>{
    const issue ={
        titulo:`issue-${faker.random.uuid()}`,
        descricao: faker.random.words(3),
        projeto:{
            nome:`projeto-${faker.random.uuid()}`,
            descricao: faker.random.words(3)
        }
    }

    const label={
        nome:`label-${faker.random.word()}`,
        color:`#ffaabb`
    }

    beforeEach(()=>{
        cy.gui_login()
        cy.api_criarIssue(issue)
            .then(response=>{
                cy.api_criarLabel(response.body.project_id, label)
                cy.visit(`${Cypress.env('user_name')}/${issue.projeto.nome}/issues/${response.body.iid}`)
            })
    })

    it(`add label issue sucesso`, ()=>{
        cy.gui_setLabelOnIssue(label)

        cy.get('.qa-labels-block').contains(label.nome)
        cy.get('.qa-labels-block span').should('have.attr', 'style', `background-color: ${label.color}; color: #333333;`)
    })

})