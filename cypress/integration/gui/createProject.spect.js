///<reference types="Cypress" />

const faker = require('faker');


describe('Criação de projetos', ()=>{
    beforeEach(() => cy.gui_login())

    it('Realizar criacao de Projeto com sucesso', ()=>{
        const projeto = {
            nome: `project-${faker.random.uuid()}`,
            descricao: faker.random.words(5) 
        }

        cy.gui_criarProjeto(projeto)
        cy.url().should('be.equal',`${Cypress.config('baseUrl')}${Cypress.env('user_name')}/${projeto.nome}`)
        cy.contains(projeto.nome).should('be.visible')
        cy.contains(projeto.descricao).should('be.visible')

    })
})
